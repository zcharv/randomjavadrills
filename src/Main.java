import com.sun.tools.classfile.Opcode;

import java.lang.reflect.Array;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        int output;
        output = nextPrime(1000);
        System.out.println(output);

        String[] words = {"Hello","Word","Jeff","Zach"};
        scrabble(words);
        System.out.println(scrabble(words));

        System.out.print(fib()+ "\n");

        System.out.print(FizzBuzz()+ "\n");

//        char[] list = {'a', 'd', 'd', 'a', 'e', 'p', 'a', 'r'};
//        char letter = mostFreq(list);
//        System.out.println(mostFreq(letter));

        String toBeSorted = "hello my name is zach";
        System.out.print(alpha(toBeSorted+ "\n"));

        String toBeReversed = "zach";
        System.out.print(reverse(toBeReversed+"\n"));

        String toBeChanged = "hello";
        System.out.print(vowel(toBeChanged+ "\n"));



    }



//    public static int prime(int input) {
//        boolean prm = false;
//        int num;
//        input++;
//
//        while (!prm){
//            num = 0;
//            for(in)
//            if num ==
//
//        }
//        return output;
//    }

    public static int nextPrime(int input){
        int counter;
        input++;
        while(true){
            counter = 0;
            for(int i = 2; i <= Math.sqrt(input); i++){
                if(input % i == 0)  counter++;
            }
            if(counter == 0)
                return input;
            else{
                input++;
            }
        }
    }

    public static int scrabble(String[] words) {

       int maxValue = 0;
       int score = 0;

        for(int i = 0; i<words.length; i++)
        {
            for(int j=0; j<words[i].length(); j++) {
                switch (words[i].charAt(j)) {
                    case 'A':
                    case 'a':
                    case 'E':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'O':
                    case 'U':
                    case 'u':
                    case 'L':
                    case 'l':
                    case 'N':
                    case 'n':
                    case 'S':
                    case 's':
                    case 'T':
                    case 't':
                    case 'R':
                    case 'r':
                        score += 1;
                        break;
                    case 'D':
                    case 'd':
                    case 'G':
                    case 'g':
                        score += 2;
                        break;
                    case 'B':
                    case 'b':
                    case 'C':
                    case 'c':
                    case 'M':
                    case 'm':
                    case 'P':
                    case 'p':
                        score += 3;
                        break;
                    case 'F':
                    case 'f':
                    case 'H':
                    case 'h':
                    case 'V':
                    case 'v':
                    case 'W':
                    case 'w':
                    case 'Y':
                    case 'y':
                        score += 4;
                        break;
                    case 'K':
                    case 'k':
                        score += 5;
                        break;
                    case 'J':
                    case 'j':
                    case 'X':
                    case 'x':
                        score += 8;
                        break;
                    case 'Q':
                    case 'q':
                    case 'Z':
                    case 'z':
                        score += 10;
                        break;
                }
            }

            if(score < maxValue) {
                score = 0;
            }
                else{
                    maxValue = score;
                    score = 0;
                }
            }
        return maxValue;
    }

    public static int fib() {
        int n = 9;
        int t1 = 0;
        int t2 = 1;
        int sum;

        for (int i=1; i<n; i++) {

            sum = t1 + t2;
            t1 = t2;
            t2 = sum;
        }
        return t2;
    }


    public static String FizzBuzz() {
        String F = "Fizz";
        String B = "Buzz";
        String FB = "FizzBuzz";
        String ans = " ";
        int n = 15;
        for(int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) ans = ans + i + FB + " ";
            else if (i % 3 == 0)
                ans = ans + i + F + " ";
            else if (i % 5 == 0)
                ans = ans + i + B + " ";
            }
        return ans;
    }

    public static char mostFreq(char[] list) {
        char mostChar = 'a';
        HashMap <Character, Integer> countTracker = new HashMap<>();
        int value;
        for(char letter:list){
            if(!countTracker.containsKey(letter)){
                countTracker.put (letter, 1);
            }else{
                value = countTracker.get(letter)+1;
                  countTracker.replace(letter, value);

            }
        }
        int highValue=0;
        char highLetter='x';
        for (Map.Entry<Character,Integer> entry: countTracker.entrySet()){
            if(highValue<entry.getValue()){
                highValue=entry.getValue();
                highLetter=entry.getKey();
            }
        }
        return highLetter;

    }

    public static String alpha(String toBeSorted) {
        char temp[] = toBeSorted.toCharArray();

        Arrays.sort(temp);
        return new String(temp);
    }

    public static String reverse(String toBeReversed){
        String temp2 = "";
        for(int i = toBeReversed.length()-1; i>=0; i--) {
            temp2 += toBeReversed.charAt(i);
        }
        return temp2;
    }

    public static char[] vowel(String toBeChanged){
        char[] temp3 = toBeChanged.toCharArray();
        for(int i = 0; i<toBeChanged.length(); i++) {
            if (temp3[i] == 'a')
                temp3[i] = '1';
            else if (temp3[i] == 'e')
                temp3[i] = '2';
            else if (temp3[i] == 'i')
                temp3[i] = '3';
            else if (temp3[i] == 'o')
                temp3[i] = '4';
            else if (temp3[i] == 'u')
                temp3[i] = '5';
            }
        return temp3;
    }

}
